class Page < ApplicationRecord
  belongs_to :site
  has_many :page_images, :dependent => :destroy
end
