class TemplateImage < ApplicationRecord
	belongs_to :template

	has_attached_file :image,
	:path => ":rails_root/public/templates/images/:id_:filename",
	:url  => "/templates/images/:id_:filename"

	validates_attachment :image,
	:content_type => { :content_type => ["image/jpg","image/jpeg", "image/gif", "image/png"] }	
end
