class PageImage < ApplicationRecord
  	belongs_to :page
 	
	has_attached_file :image,
	:path => ":rails_root/public/images/:id_:filename",
	:url  => "/images/:id_:filename"

	validates_attachment :image,
	:content_type => { :content_type => ["image/jpg","image/jpeg", "image/gif", "image/png"] }	
end
