json.extract! page, :id, :name, :template, :remote_storage, :data, :site_id, :created_at, :updated_at
json.url page_url(page, format: :json)
