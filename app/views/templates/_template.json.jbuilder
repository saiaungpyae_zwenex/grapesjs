json.extract! template, :id, :name, :template, :data, :site_id, :created_at, :updated_at
json.url template_url(template, format: :json)
