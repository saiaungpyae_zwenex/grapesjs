json.extract! site, :id, :name, :template, :data, :user_id, :created_at, :updated_at
json.url site_url(site, format: :json)
