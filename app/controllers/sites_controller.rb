class SitesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_site, only: [:show, :edit, :update, :destroy]

  # GET /sites
  # GET /sites.json
  def index
    @sites = Site.where(user_id: current_user)
  end

  # GET /sites/1
  # GET /sites/1.json
  def show    
    @pages = Page.where(site_id: params[:id])
  end

  # GET /sites/new
  def new
    @site = Site.new
    @templates = Template.all.select("id", "name");    
  end

  # GET /sites/1/edit
  def edit
  end

  # POST /sites
  # POST /sites.json
  def create
    @site = Site.new(site_params)
    @site.user_id = current_user.id
    template_id = params[:site][:template_id]
    unless(template_id.nil? || template_id.empty?)
      template = Template.find(template_id)
      @page = Page.new
      @page.site = @site
      @page.name = "Page1"
      @page.template = template.template
      @page.remote_storage = template.remote_storage

      @page.save!

    end

    respond_to do |format|
      if @site.save
        format.html { redirect_to site_pages_path(@site), notice: 'Site was successfully created.' }
        format.json { render :show, status: :created, location: @site }
      else
        format.html { render :new }
        format.json { render json: @site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sites/1
  # PATCH/PUT /sites/1.json
  # def update
  #   respond_to do |format|
  #     if @site.update(site_params)
  #       format.html { redirect_to @site, notice: 'Site was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @site }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @site.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # DELETE /sites/1
  # DELETE /sites/1.json
  def destroy
    @site.destroy
    respond_to do |format|
      format.html { redirect_to sites_url, notice: 'Site was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site
      @site = Site.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_params
      params.require(:site).permit(:name)
    end
end
