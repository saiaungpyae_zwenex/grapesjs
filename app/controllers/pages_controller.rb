class PagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  def upload_image
    if params[:files]
      image_file = params[:files][0]
      page_id = params[:page_id]
      image = PageImage.new(:page_id => page_id, :image => image_file)

      respond_to do |format|
        if image.save
          data = { :data => '/images/'+image.id.to_s+"_"+image.image_file_name}
          format.json  { render :json => data }
        end
      end
    end
  end

  def delete_image    
    @image =  PageImage.find(params[:image_id])
    @image.destroy
  end

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.where(site_id: params[:site_id])
  end

  def get_template
    render json: Page.find(params[:id]).template
  end

  def get_remote
    render json: Page.find(params[:id]).remote_storage
  end

  def store_template
    data = Page.find(params[:id])
    data.update_attributes(template: params[:template])
  end

  def store_remote
    @data = Page.find(params[:id])
    @data.update_attributes(remote_storage: params[:data].to_json)
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    @page_id = Page.find(params[:id]).id
    @linkto = Site.find(params[:site_id]).pages.select("site_id", "id", "name")
    @images = PageImage.where(page_id: @page_id).select("id", "image_file_name").order(created_at: :desc)
  end

  def show_preview
    @page = Page.find(params[:id])
    render :layout => false  
  end

  # GET /pages/new
  def new
    @site = Site.find(params[:site_id])
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)    
    respond_to do |format|
      if @page.save
        format.html { redirect_to site_pages_path(params[:site_id]), notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to site_pages_path(params[:site_id]), notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to site_pages_path(params[:site_id]), notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:name, :site_id)
    end
end
