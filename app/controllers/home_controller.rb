class HomeController < ApplicationController  

  def index
  end

  def getdata
  	render json: Template.find(params[:id]).data
  end

  def store
  	data = Template.find(params[:id])

  	data.update_attributes(data: params[:data].to_json)
  end

  private
    def set_params
      @param = Template.find(params[:id])
    end
end
