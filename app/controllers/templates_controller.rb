class TemplatesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_template, only: [:show, :edit, :update, :destroy]


  def upload_image
    files = params[:files]
    if files
      image_file = files[0]
      template_id = params[:template_id]
      image = TemplateImage.new(:template_id => template_id, :image => image_file)

      respond_to do |format|
        if image.save
          data = { :data => '/templates/images/'+image.id.to_s+"_"+image.image_file_name}
          format.json  { render :json => data }
        end
      end
    end
  end

  def delete_image    
    @image =  TemplateImage.find(params[:image_id])
    @image.destroy
  end
  # GET /templates
  # GET /templates.json
  def index
    @templates = Template.all
  end

  def get_remote
    render json: Template.find(params[:id]).remote_storage
  end

  def store_remote
    @data = Template.find(params[:id])
    @data.update_attributes(remote_storage: params[:data].to_json)
  end

  def store_template
    data = Template.find(params[:id])
    data.update_attributes(template: params[:template])
  end

  # GET /templates/1
  # GET /templates/1.json
  def show_preview
    @template = Template.find(params[:id])
    render :layout => false 
  end

  def show
    @template_id = Template.find(params[:id]).id
    @images = TemplateImage.where(template_id: @template_id).select("id", "image_file_name").order(created_at: :desc)    
  end

  # GET /templates/new
  def new    
    @template = Template.new
  end

  # GET /templates/1/edit
  def edit    
  end

  # POST /templates
  # POST /templates.json
  def create    

    @template = Template.new(template_params)    
    respond_to do |format|
      if @template.save
        format.html { redirect_to template_path(@template), notice: 'Template was successfully created.' }
        format.json { render :show, status: :created, location: @template }
      else
        format.html { render :new }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /templates/1
  # PATCH/PUT /templates/1.json
  def update
    respond_to do |format|
      if @template.update(template_params)
        format.html { redirect_to @template, notice: 'Template was successfully updated.' }
        format.json { render :show, status: :ok, location: @template }
      else
        format.html { render :edit }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /templates/1
  # DELETE /templates/1.json
  def destroy
    @template.destroy
    respond_to do |format|
      format.html { redirect_to @template, notice: 'Template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def template_params
      params.require(:template).permit(:name)
    end
end
