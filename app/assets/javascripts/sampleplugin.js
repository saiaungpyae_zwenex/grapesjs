grapesjs.plugins.add('sample-plugin', function(editor, options){
  var blockManager = editor.BlockManager;
  blockManager.add("form", {
                label: '\n      <svg class="gjs-block-svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n        <path class="gjs-block-svg-path" d="M22,5.5 C22,5.2 21.5,5 20.75,5 L3.25,5 C2.5,5 2,5.2 2,5.5 L2,8.5 C2,8.8 2.5,9 3.25,9 L20.75,9 C21.5,9 22,8.8 22,8.5 L22,5.5 Z M21,8 L3,8 L3,6 L21,6 L21,8 Z" fill-rule="nonzero"></path>\n        <path class="gjs-block-svg-path" d="M22,10.5 C22,10.2 21.5,10 20.75,10 L3.25,10 C2.5,10 2,10.2 2,10.5 L2,13.5 C2,13.8 2.5,14 3.25,14 L20.75,14 C21.5,14 22,13.8 22,13.5 L22,10.5 Z M21,13 L3,13 L3,11 L21,11 L21,13 Z" fill-rule="nonzero"></path>\n        <rect class="gjs-block-svg-path" x="2" y="15" width="10" height="3" rx="0.5"></rect>\n      </svg>\n      <div class="gjs-block-label">Form</div>',
                category: "Forms",
                content: '\n        <form class="form">\n          <div class="form-group">\n            <label class="label">Name</label>\n            <input placeholder="Type here your name" class="input"/>\n          </div>\n          <div class="form-group">\n            <label class="label">Email</label>\n            <input type="email" placeholder="Type here your email" class="input"/>\n          </div>\n          <div class="form-group">\n            <label class="label">Gender</label>\n            <input type="checkbox" class="checkbox" value="M">\n            <label class="checkbox-label">M</label>\n            <input type="checkbox" class="checkbox" value="F">\n            <label class="checkbox-label">F</label>\n          </div>\n          <div class="form-group">\n            <label class="label">Message</label>\n            <textarea class="textarea"></textarea>\n          </div>\n          <div class="form-group">\n            <button type="submit" class="button">Send</button>\n          </div>\n        </form>\n      '
  });

  blockManager.add("input", {
                label: '\n      <svg class="gjs-block-svg" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">\n        <path class="gjs-block-svg-path" d="M22,9 C22,8.4 21.5,8 20.75,8 L3.25,8 C2.5,8 2,8.4 2,9 L2,15 C2,15.6 2.5,16 3.25,16 L20.75,16 C21.5,16 22,15.6 22,15 L22,9 Z M21,15 L3,15 L3,9 L21,9 L21,15 Z"></path>\n        <polygon class="gjs-block-svg-path" points="4 10 5 10 5 14 4 14"></polygon>\n      </svg>\n      <div class="gjs-block-label">' + "Input" + "</div>",
                category: "Forms",
                content: '<input class="input"/>'
  });
  blockManager.add('productListing', {
      label: 'Product',
      content: {content: '<figure class="product-card">Helo</figure>',
                style: {
                  "max-width":'280px',                  
                  "font-size":'12px',
                  "margin":'5px',
                  "box-shadow":'2px 2px 15px #999'
                  }
                },
      attributes: {
        class: 'fa fa-user',
        title: 'Product Block'
      },
      category: 'Basic',
      CssComposer: {
         rules: '.product-card{ color: red}',
      }
    });
  var cssComposer = editor.CssComposer;
  var sm = editor.SelectorManager;
  var sel1 = sm.add('product-card');
  var rule = cssComposer.add([sel1]);
  rule.set('style', {
    "max-width":"280px",
    "font-size":"12px",
    "margin":"5px",
    "box-shadow":"2px 2px 15px #999",
  });
});