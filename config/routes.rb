Rails.application.routes.draw do

	post '/pages/uploadimages' => "pages#upload_image"
	post '/pages/deleteimage' => "pages#delete_image"

	post '/templates/uploadimages' => "templates#upload_image"
	post '/templates/deleteimage' => "templates#delete_image"

	get '/templates/:id/preview' => "templates#show_preview", as: :template_preview
	get '/templates/:id/remote_storage'  => "templates#get_remote"
	post '/templates/:id/remote_storage' => "templates#store_remote"
  	post '/templates/:id/save' => "templates#store_template", as: :save_template

  	resources :templates
  	
  	resources :sites do
		get '/pages/:id/preview' => "pages#show_preview", as: :show_preview
		get '/pages/:id/template' =>  "pages#get_template", as: :get_page
		post '/pages/:id/template' =>  "pages#store_template", as: :save_page
		get '/pages/:id/remote_storage'  => "pages#get_remote"
		post '/pages/:id/remote_storage' => "pages#store_remote"
	  	resources :pages
	end

	devise_for :users

	get 'home/index'

	root :to => "home#index"
end
