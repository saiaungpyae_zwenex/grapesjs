class CreateTemplateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :template_images do |t|
      t.references :template, foreign_key: true

      t.timestamps
    end
  end
end
