class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :name
      t.text :template
      t.json :remote_storage
      t.json :data
      t.references :site, foreign_key: true

      t.timestamps
    end
  end
end
