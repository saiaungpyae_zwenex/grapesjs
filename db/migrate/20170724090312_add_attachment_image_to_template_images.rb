class AddAttachmentImageToTemplateImages < ActiveRecord::Migration[5.1]
  def self.up
    change_table :template_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :template_images, :image
  end
end
