class AddAttachmentImageToPageImages < ActiveRecord::Migration[5.1]
  def self.up
    change_table :page_images do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :page_images, :image
  end
end
