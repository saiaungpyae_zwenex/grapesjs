class CreatePageImages < ActiveRecord::Migration[5.1]
  def change
    create_table :page_images do |t|
      t.references :page, foreign_key: true

      t.timestamps
    end
  end
end
